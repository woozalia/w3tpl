<?php namespace w3tpl;
/*
  USAGE: This file needs to be in the root of the extension's folder
    so that it can accurately determine said folder's path (without any
    dirname() tweaking or whatever).
    
    There may be proper API ways to do these things, but I haven't been able
      to figure it out and the documentation doesn't have much in the way of
      "how to" at the API level.
*/

use ferret as F;

class cExtension extends F\mw\caExtension {
  
    // ++ SETUP ++ //
  
    public function __construct() { $this->EnforceMonogeny(); }
    // REMINDER: EnforceMonogeny() must be called from constructor
    static protected function Call_EnforceMonogeny(){}
    
    // -- SETUP -- //
    // ++ CONFIG ++ //
    
    public function MyFolder() : string { return __DIR__; }

    // -- CONFIG -- //
}
