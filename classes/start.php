<?php namespace w3tpl;
/*
  HISTORY:
    2022-04-05 Split start.php off from app.php, hoping to resolve dependency issue.
*/
use ferret\classloader as FC;

class cEntryPoint {

    // ++ SETUP ++ //
  
    static public function cbInit(\Parser $mwoParser) {
        self::Setup();
        
        if (cOptions::GetProvideSMWSupport()) {
            cApp::Me()->SetDatabaseClass('fcDataConn_SMW');	// use db class with SMW support
        }

        $fpExt = self::GetExtFolder();
        
        // load all tags
        $fpTags = $fpExt.'/tags/enabled';
        caTag::LoadAll($fpTags,$mwoParser);
        
        // set full path for plugins
        xcModule::SetFolderPath($fpExt.'/plugins');	// same (maybe change to "modules", though)
        
        return TRUE;
    }
    static public function Setup() {
        // get base folder for the extension
        $fpExt = dirname(__DIR__);
        self::SetExtFolder($fpExt);
        
        // setup Ferreteria
        require_once $fpExt.'/config/links/ferreteria/start.php';
        /*
          NOTE: 
            2022-04-06 require_once is a bit of a kluge. I can't confirm
            that init() is being called more than once -- but if I
            use require instead of require_once, we get a duplicate
            class declaration.
        */
        
        // register needed Ferreteria libraries
        FC\csLoader::LoadLibrary('ferret.mw');
        FC\csLoader::LoadLibrary('ferret.data');
        
    }
    
    // -- SETUP -- //
    // ++ INTERNAL DATA ++ //
    
    // folder for this extension
    static private $fpExt;
    static protected function SetExtFolder(string $fp) { self::$fpExt = $fp; }
    static protected function GetExtFolder() : string { return self::$fpExt; }
        
    // -- INTERNAL DATA -- //
}
