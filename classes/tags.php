<?php namespace w3tpl;
/*
  PURPOSE: Base class for w3tpl tags
  HISTORY:
    2017-10-29 started
  TODO: The argument functions should be replaced by a method that returns an fcInputData object.
*/

abstract class caTag {

    // ++ EVENT ++ //

    abstract public function Go() : string;

    // -- EVENT -- //
    // ++ GLOBAL ++ //
    
    // ACTION: load all tag files found in the tag handlers folder ($fpTags)
    static public function LoadAll($fpTags, \Parser $mwoParser) {
        $poDir = dir($fpTags);		// dir() returns a native PHP object
        while (FALSE !== ($fnFile = $poDir->read())) {
            if (($fnFile!='.') && ($fnFile!='..')) {	// TODO: this is probably redundant - these would also fail !is_dir()
                $fs = $fpTags.'/'.$fnFile;
                if (!is_dir($fs)) {	// ignore subfolders
                    $fnLower = strtolower($fnFile);
                    // check for .php extension
                    if (substr($fnLower,-4) == '.php') {
                        $sTagName = substr($fnLower,0,strlen($fnLower)-4);
                        
                        global $wgAutoloadClasses;
                        
                        // tag name found - process it:
                        $sClass = 'w3tpl\\tag\\c_'.$sTagName;
                        $sFx = $sClass.'::Call';
                        $wgAutoloadClasses[$sClass] = $fs;
                        if (is_callable($sFx)) {
                            $mwoParser->setHook( $sTagName,$sFx );
                            self::RegisterTag($sTagName,$sClass);
                        } else {
                            throw new \exception("w3tpl internal error: '$sFx' cannot be called (for tag <$sTagName>).");
                        }
                    }
                }
            }
        }
        $poDir->close();
    }
    
    static private $arTags = [];
    static protected function RegisterTag($sName, $sClass) { self::$arTags[$sName] = $sClass; }
    static public function TagsRegistered() : array { return self::$arTags; }
    
    // -- GLOBAL -- //
    // ++ ENTRY POINT ++ //

    static public function Call($sInput, array $arArgs, \Parser $mwoParser = NULL, $mwoFrame = FALSE) {
        $sClass = get_called_class();	// static equivalent for get_class($this)
        $xoTag = new $sClass;
        $xoTag->Setup((string) $sInput,$arArgs,$mwoParser,$mwoFrame);
        
        return $xoTag->FigureReturnValue();
    }
    public function Setup(string $sInput, array $arArgs, \Parser $mwoParser = NULL, $mwoFrame = FALSE) {
        $this->SetInput($sInput);
        $this->SetArguments($arArgs);
        $this->SetParser($mwoParser);
        // not even sure what Frame is for, so not doing anything with it
    }
    
    // -- ENTRY POINT -- //
    // ++ CONFIG ++ //
    
    abstract protected function TagName();	// maybe there's a way to get this from MW?
    
    // -- CONFIG -- //
    // ++ INPUT ++ //
    
    // this is specificall the tag contents; TODO: perhaps rename
    private $sInput;
    protected function SetInput(string $s) { $this->sInput = $s; }
    protected function GetInput() : string { return $this->sInput; }
    
    private $mwoParser;
    protected function SetParser(\Parser $mwo) { $this->mwoParser = $mwo; }
    protected function GetParser() : \Parser { return $this->mwoParser; }
    
    // input arguments have their own section
    
    // + INPUT: ARGUMENTS + //
    
    private $arArgs;
    protected function SetArguments(array $arArgs) { $this->arArgs = $arArgs; }
    protected function GetArguments() : array { return $this->arArgs; }
    protected function ArgumentExists($sName) : bool { return array_key_exists($sName,$this->arArgs); }
    protected function ArgumentValue($sName) { return $this->arArgs[$sName]; }
    protected function ArgumentValueNz(string $sName,$sDefault=NULL) {
        if ($this->ArgumentExists($sName)) {
            $out = $this->ArgumentValue($sName);
        } else {
            $out = $sDefault;
        }
        return $out;
    }
    protected function RequireArgument($sName) {
        if ($this->ArgumentExists($sName)) {
            $sOut = $this->ArgumentValue($sName);
        } else {
            $sTag = $this->TagName();
            $this->AddError("&lt;$sTag&gt; tag needs a '$sName' attribute.");
            $sOut = NULL;
        }
        return $sOut;
    }
    
    // -- INPUT -- //
    // ++ OPTIONS ++ //
    
    private $bIsolateOutput = FALSE;
    protected function SetIsolateOutput(bool $b) { $this->bIsolateOutput = $b; }
    protected function GetIsolateOutput() : bool { return $this->bIsolateOutput; }
    // TODO 2022-04-22: document what this does
    protected function GetMarkerType() { return $this->GetIsolateOutput() ? 'nowiki' : NULL; }
    // TODO 2022-04-22: document what this does
    protected function FigureReturnValue() {
        $sOut = $this->Go();
        $sMarker = $this->GetMarkerType();
        if (is_null($sMarker)) {
            return $sOut;
        } else {
            return [
              $sOut,
              "markerType" => $sMarker
              ];
        }
    }
    
    // -- OPTIONS -- //
    // ++ OUTPUT ++ //
    
    private $sMsg = NULL;
    private $isOk = TRUE;
    protected function AddMessage($sMsg) { $this->sMsg .= $sMsg; }
    protected function AddError($sMsg) {	// TODO: formatting to make output more obviously an error
        $this->AddMessage($sMsg);
        $this->isOk = FALSE;
    }
    public function GetMessages() { return $this->sMsg; }
    protected function IsOkay() { return $this->isOk; }
    
    // -- OUTPUT -- //
    // ++ DEBUGGING ++ //
    
    public function DumpTag() : string {
        $sTagName = $this->TagName();
        $arArgs = $this->GetArguments();
        $sArgs = '';
        foreach ($arArgs as $sName => $sVal) {
            $sArgs .= " $sName=$sVal";
        }
        return "&lt;$sTagName$sArgs&gt;";
    }
    
    // -- DEBUGGING -- //
}
abstract class caTag_basic extends caTag {

    // ++ EVENT ++ //

    // CEMENT caTag
    public function Go() : string {
        $this->GetTagOptions();
        $sInRaw = $this->GetTagInput();
        #echo 'tag '.$this->DumpTag()."<br>";
        $sInPrep = $this->ProcessTagInput($sInRaw);
        $sOut = $this->UseTagInput($sInPrep);
        #echo "tag [".$this->TagName()." output: [$sOut]<br>";
        return $sOut;
    }

    // -- EVENT -- //
    // ++ PROCESSING ++ //
    
    /*----
      STUB
      PHASE I: OPTIONS
    */
    protected function GetTagOptions() {}
    // PHASE II: INPUT SOURCE
    abstract protected function GetTagInput() : string;
    // PHASE III: INPUT PROCESSING
    protected function ProcessTagInput(string $sInput) : string { return $sInput; }
    // PHASE IV: INPUT USAGE
    abstract protected function UseTagInput(string $sInput) : string;

    // -- INPUT -- //
}
