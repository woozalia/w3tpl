<?php namespace w3tpl;
/*
  PURPOSE: base class for tags that handle variables
  REMEMBER:
    Values are only stored in the tag object when the tag-processing code puts them there.
      We therefore do not need a separate array to keep track of "old" values in order to
      allow current tag data to operate on them.
  HISTORY:
    2017-11-01 started
    2022-03-24 cleaning up disabled stuff
*/
abstract class caTag_Var extends caTag_basic {
  
    // ++ PHASE I: OPTIONS ++ //
 
    // UNSTUB: caTag_basic
    protected function GetTagOptions() {
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
              case 'format':
                $this->SetFormat($sVal);
                break;
            }
        }
    }
    
    // FUTURE: might need different defaults for different variable types
    private $sFormat = 'Y/m/d H:i:s'; // default time format
    protected function SetFormat(string $s) { $this->$sFormat = $s; }
    protected function GetFormat() : string { return $this->sFormat; }
    
    // -- PHASE I: OPTIONS -- //
    // ++ PHASE II: INPUT SOURCE ++ //
  
    /*----
      ACTION: get the raw input to be processed
      NOTE: For binary operations like APPEND, the current value
        will be the first operand and the input will be the *second* operand
        "self append" will append the current value to itself.
      HISTORY:
        2020-10-15 Rewriting MainProcess() to make some sort of sense, as far as compartmentalization
        2022-03-24 set default value (empty string) in case of no arguments
        2022-04-20 moved from <let> tag to the variable-tag parent class - except 'self' needs to be
          <let>-only (<get> *must* start with its input value)
    */
    protected function GetTagInput() : string {
        $sInput = '';

        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            $isFnd = TRUE;
            switch ($sName) {
              case 'arg':
                $sInput = $this->GetTagInput_arg($sVal);
                break;
              case 'chr':
                $sInput = chr($sVal);
                break;
              case 'copy':
                $sInput = $this->GetTagInput_copy($sVal);
                break;
              case 'intfx': // execute internal function
                $sInput = $this->GetTagInput_intFx($sVal);
                break;
              case 'val':
                $sInput = csVar::GetValue_fromExpression($sVal);
                break;
              default:
                $isFnd = FALSE;
            }
            // return on first match
            if ($isFnd) {
                return $sInput;
            }
        }
          
        // if no matches, starting value is just tag-pair's contents
        return $this->GetInput();
    }
    /*----
      USAGE: only called from GetTagInput()
      INPUT:
        $sCopyName = name of variable to copy
    */
    protected function GetTagInput_copy(string $sCopyName) : string {
        $sInput = ''; // default in case we can't retrieve a value
        $osCopyValue = csVar::VariableStatus($sCopyName);  // get status of named variable
        if ($osCopyValue->HasIt()) {
            // a value has been found: get it
            $sInput = $osCopyValue->GetItNz();  // NULL -> empty string
        }
        return $sInput;
    }
    /*----
      USAGE: only called from GetTagInput()
      INPUT:
        $sReqName = name of request variable to copy
    */
    protected function GetTagInput_arg(string $sReqName) : string {
        global $wgRequest;
        
        #$this->GetParser()->disableCache();       // page needs to respond dynamically: no caching
        // 2022-04-12 disableCache() is apparently no longer a thing
        
        $sInput = $wgRequest->getVal($sReqName);  // get the value of the named Request argument
        return is_null($sInput) ? '' : $sInput;
    }
    protected function GetTagInput_intFx(string $sFxName) : string {
        $sOut = '';
        switch ($sFxName) {
          case 'time':
            $sFmt = $this->GetFormat();
            $sOut = date($sFmt);
            break;
        }
        return $sOut;
    }
    
    // -- PHASE II: INPUT SOURCE -- //
    // ++ PHASE III: INPUT PROCESSING ++ //
    
    protected function ProcessTagInput(string $sInput) : string {
        $sOut = $sInput;  // default is to pass through
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
              case 'astag':
                // Tag markup is only applied to what's being appended.
                $sTrimmed = trim($sInput);
                $sOut = "<$sTrimmed>";
                break;
            }
        }
        return $sOut;
    }
    
    // -- PHASE III: INPUT PROCESSING -- //
    
    /*----
      ACTION: do any further processing needed on the input
      HISTORY:
        2020-10-15 Rewriting MainProcess() to make some sort of sense, as far as compartmentalization
        2022-04-09
          * There should only be output if the "echo" attribute is present; now checks for that.
          * Moved setting-of-variable from Go() to here.
        2022-04-20 moved from <let> tag to the variable-tag parent class, because any tag that
          processes input should be able to use these.
        2022-05-02 There's a problem with "astag" when combined with "append" in that the markup
          was being applied to the entire accumulated value. Fixing this requires some thinking,
          as the *new* value needs to be kept somewhere before being added to the variable's value.
    */
    protected function UseTagInput(string $sInput) : string {
        $sOut = $sInput;

        $doDebug = FALSE;
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
              case 'append':
                $sOut = $this->UseTagInput_append($sOut);
                break;
              case 'newline':
                $sOut .= "\n";
                break;
              case 'debug':
                $sOut = $this->DumpTag()."&rarr;[$sOut]";
                echo "SOUT(1) = [$sOut]<br>";
                $doDebug = TRUE;
                break;
              case 'lcase':
                $sOut = strtolower($sOut);
                break;
              case 'lcfirst':
                $sOut = lcfirst($sOut);
                break;
              case 'len':
                if (is_numeric($sVal)) {
                    $sOut = substr($sOut,0,$sVal);
                }
                break;
              case 'parse':
              case 'pre':
                $sOut = $this->UseTagInput_parse($sOut);
                break;
              case 'trim':
                $sOut = trim($sOut);
                break;
              case 'ucase':
                $sOut = strtoupper($sOut);
                break;
              case 'ucfirst':
                $sOut = ucfirst($sOut);
                break;
            }
        }
        if ($doDebug) {
            echo "SOUT(2) = [$sOut]<br>";
        }
        return $sOut;
    }
    protected function UseTagInput_append(string $sInput) : string {
        $oVar = $this->GetVariableObject();
        $sValue = $oVar->GetValue();
        return $sValue.$sInput;	// append processed input to the starting value
    }
    protected function UseTagInput_parse(string $sInput) : string {
        $mwoParser = $this->GetParser();
        return $mwoParser->recursiveTagParse($sInput);
    }
  
    // ++ PHASE IV: INPUT USAGE ++ //
    // -- PHASE IV: INPUT USAGE -- //
    
}
