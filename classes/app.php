<?php namespace w3tpl;
/*
  HISTORY:
    2017-10-29 split off from w3tpl.body.php
    2017-11-09 added $wgW3xAllowRawHTML option
      To allow only on protected pages:
	$wgW3xAllowRawHTML = $wgTitle->isProtected ('edit');
      To allow on all pages (e.g. on a site where only approved members can edit):
	$wgW3xAllowRawHTML = TRUE;
    2018-01-26 added $wgW3xSupportSMW option
    2018-07-31 added $wgW3xStrictMode option
    2020-08-14 changing globals to a static function pairs:
      $wgW3xStrictMode -> S/GetIsStrictMode()
      $wgW3xAllowRawHTML -> S/GetAllowRawHTML()
      $wgW3xSupportSMW -> S/GetProvideSMWSupport()
*/

use ferret as F;
use ferret\classloader as FC;

class cApp extends F\mw\cApp {

    // ++ FRAMEWORK ++ //
    
    private $oMod;
    public function SetModule(xcModule $oModule) { $this->oMod = $oModule; }
    public function GetModule() { return $this->oMod; }
    public function GetDataHelper() { return $this->GetModule()->GetDataHelper(); }
    
    // -- FRAMEWORK -- //
}
