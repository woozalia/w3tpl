<?php namespace w3tpl;
/*
  PURPOSE: class for managing tag-variables in w3tpl
  RULES:
    * each cVar object has a unique name that includes any array indexes it might have
    * That unique name can be used to access its storage location in the master array, or its value on the current page.
    * If the object is an array element, then the storage name follows the format 'varname/index'.
    * Variable expressions use a more natural syntax, e.g. varname[index].
    * An *expression* can be:
    ** a literal value - "this is a string"
    ** a reference to a variable - "$theVar"
    ** a reference to a system value - @row.fieldname
    ** a reference to a function's output - (not sure what the syntax should be)
    * A variable reference can be:
    ** scalar value - "$aScalar"
    ** an array - pointer to a storage location that has subvalues - should probably be like "$anArray[]"
    ** array element - "$anArray[index_expression]" where "index_expression" is an expression
    * We automatically find the value of all inner elements as needed, but leave the outermost unresolved
	in order to allow for different operations depending on context.
	
    This is not a hierarchy; all variables live in a flat array managed statically by the class.
  HISTORY:
    2012-06-03 IS THERE ANY REASON not to rename clsW3VarName to clsW3Var?
    2015-09-10 Renamed clsW3VarName to clsW3Var.
    2015-09-28 extracted from W3TPL.php
    2016-09-19 adapting for w3tpl2
    2017-10-29 take 2... renaming cw3Variable to xcVar, making things work for real...
    2017-10-30 brutally rewriting 
    2022-04-08
      * deleting deprecated functions
        * function GetVariableValue(): use VariableStatus() instead
      * splitting xcVar into:
        * w3tpl\csVar -- static services only
        * w3tpl\cVar -- dynamic functionality
      
*/

use ferret as F;

class csVar {
    // ++ REPOSITORY ++ //
    
    static private $arVars = [];
    static protected function GetVars() { return self::$arVars; }
    static protected function VariableExists($sName) : bool { return array_key_exists($sName,self::$arVars); }
    // ASSUMES: variable exists
    static protected function GetVariableObject($sName) { return self::$arVars[$sName]; }

    /*----
      HISTORY:
        2022-04-11 created for cTemplate class to use
    */
    static public function GetValues() : array {
        $arVars = self::GetVars();
        $arVals = [];
        foreach ($arVars as $sName => $oVar) {
            $sVal = $oVar->GetValue();
            $arVals[$sName] = $sVal;
        }
        return $arVals;
    }
    /*----
      HISTORY:
        2018-01-28 created; why didn't this already exist?
    */
    static protected function MakeVariableObject($sName) {
        if (self::VariableExists($sName)) {
            $oVar = self::GetVariableObject($sName);
        } else {
            $oVar = static::SpawnVariable($sName);
        }
        return $oVar;
    }
    /*----
      RETURNS: a Scalar Status with the value being the variable's value, NOT the Variable Object
      HISTORY:
        2020-08-16 created to supercede GetVariableValue()
        2022-04-22 now lowercases the name, because varnames are supposed to be case-insensitive
      THINKING: If later on it turns out that we need to be able to retrieve
        the object as well as the value, we can extend cElementResult to include that.
        For now, though, we just want the value if we can get it.
    */
    static public function VariableStatus(string $sName) : F\cScalarLocal {
        $os = new F\cScalarLocal;
        $sName = strtolower($sName);
        if (self::VariableExists($sName)) {
        
            $oVar = self::GetVariableObject($sName);
            $os->SetIt($oVar->GetValue());
        
        } else {
        
            $os->ClearIt();
            
        }
        return $os;
    }
    static public function SetVariableValue(string $sName,string $sValue) {
        if (self::VariableExists($sName)) {
            $oVar = self::GetVariableObject($sName);
        } else {
            $oVar = static::SpawnVariable($sName);
        }
        $oVar->SetValue($sValue);
    }
    static protected function DefaultClass() : string { return cVar::class; }
    /*----
      INPUT: $sName = name by which this should be saved for later use; NULL = temp variable (used for parsing)
      TODO: 2022-04-11 should probably be renamed CreateVariable()
        Why doesn't it check for an existing variable in the array? Document this.
        (If it did do that, though, it should be MakeVariable().)
    */
    static protected function SpawnVariable(string $sName=NULL) : cVar {
        $sClass = static::DefaultClass();
        $oVar = new $sClass;
        if (!is_null($sName)) {
            $oVar->SetName($sName);
            self::$arVars[$sName] = $oVar;
        }
        return $oVar;
    }
    /*----
      HISTORY:
        2020-08-16 Created as a sort of "official give me the best info" debugging tool
        2022-04-08 renamed from DumpContents() to DumpFull(), to be consistent with convention
    */
    static public function DumpFull() : string {
        $ar = self::GetVars();
        if (count($ar) > 0) {
            $out = "<ul>\n";
            foreach ($ar as $sName => $oVar) {
                $out .= $oVar->DumpSelf();
            }
            $out .= "</ul>\n";
        } else {
            $out = '<i>No variables defined.</i><br>';
        }
        return $out;
    }
    
    // -- REPOSITORY -- //
    // ++ PARSING ++ //
    
    /*----
      ACTION: creates/retrieves a variable-object from a descriptor in script syntax
        Basically, this is where it parses the "name=" value of a variable-operation tag.
      TODO: Should probably be renamed MakeVariable_fromExpression(), because it checks for existing var
        2020-08-16 ...or actually MakeVariable_fromNameValue() might be better, as I understand it.
      INPUT: $sExpr = expression that evaluates to a name
        Must be a regular (non-sysvar) variable.
        May include array offset.
        Should not include the "$".
      RETURNS: discovered or created variable object
      PUBLIC because variable-accessing tags (<let>,<get>...) need to use it
      NOTE: Does not support @vars because those are actually values, not variables.
      HISTORY:
        2017-10-30 was not working before; wasn't really written, even
        2018-01-28 removing support for arrays; brackets will be treated as part of variable name
        2022-04-20 removed commented-out array support, since it would probably need rethinking anyway
    */
    static public function GetVariable_fromExpression($sExpr,$doCreate=FALSE) : ?cVar {
        // normalize all name strings to lower case
        $sExpr = strtolower($sExpr);
        if (self::VariableExists($sExpr)) {
            #echo 'VAR EXISTS<br>';
            $oVar = self::GetVariableObject($sExpr);
            #echo 'VAR VALUE=['.$oVar->GetValue().'] DUMP:'.$oVar->DumpSelf().'<br>';
        } else {
            #echo 'VAR NOT FOUND<br>';
            if ($doCreate) {
                $oVar = static::SpawnVariable($sExpr);
            } else {
                $oVar = NULL;
            }
        }
        return $oVar;
    }
    /*----
      ACTION: takes an expression which may be a variable reference and returns its value
      INPUT: $sExpr = a value expression
        If the expression does not start with a "$", it's taken as a literal and returned verbatim.
        If the expression starts with "$", it's taken as a variable reference and
          the remainder is passed to GetVariable_fromExpression(); the return from there is
          then queried to get its value.
      RETURNS: string
      PUBLIC so tags can use it
      HISTORY:
        2017-10-30 written from scratch 
          I think there must have been a similar function *somewhere* in old w3tpl...)
        2017-11-05 made public for <call> tag
        2018-01-23 call to GetVariable_fromExpression() in '$' had no argument!
        2018-07-31 allowing for nonexistent variable if not in strict mode
    */
    static public function GetValue_fromExpression(string $sExpr) {
        $ch = substr($sExpr,0,1);
        switch ($ch) {
          case '$':
            $sVar = substr($sExpr,1);	// remainder after '$'
            $oVar = static::GetVariable_fromExpression($sVar);
            if (is_null($oVar)) {
                if (cOptions::GetIsStrictMode()) {
                    echo "Could not find variable for expression [<b>$sVar</b>].";
                    throw new exception('W3TPL internal error');
                } else {
                    $rtn = NULL;
                }
            } else {
                $rtn = $oVar->GetValue();
            }
            break;
          case '@':
            $sFxName = substr($sExpr,1);
            $rtn = self::GetValue_fromSysFunction($sFxName);
            break;
          default:
            $rtn = $sExpr;
        }
        return $rtn;
    }
    /*----
      HISTORY:
        2017-11-14 adapting from W3GetSysData()
        2020-08-17 recovered @user.* fx from commented section
        2022-04-19 adding @w3ext fx
        2022-04-20 realized that @w3ext.formurl was unnecessary
          when we can use {{localurl:SpecialPage:W3TPL}}. Removed formurl.
    */
    static protected function GetValue_fromSysFunction(string $sName) {
        global $sql;

        $out = '';

        $arParts =  explode('.', $sName);
        if (isset($arParts[1])) {
            $sParamRaw = $arParts[1];
            $sParam = strtolower($sParamRaw);
        } else {
            $sParam = NULL;
        }
        #echo 'PARTS: '.F\csArray::Render($arParts);
        $sPart0 = $arParts[0];
        //$wgOut->addHTML("FX: [$sPart0] REQ: [$sParam] <br>");

        switch ($sPart0) {
          case 'title':
            global $wgTitle;
            switch ($sParam) {
              case 'id':
                $out = $wgTitle->getArticleID();
                break;
              case 'full':	// namespace:subject
                $out = $wgTitle->getPrefixedText();
                //$wgOut->addHTML("FULL TITLE: [$out]<br>");
                break;
              case 'subject':	// just the part after the namespace and before any slashes
                $out = $wgTitle->getBaseText();
                break;
              case 'name':		// just the part after the namespace
                $out = $wgTitle->getText();
                break;
              case 'url':
                $out = $wgTitle->getFullURL();
                break;
              case 'dbkey':		// as stored in db tables
                $out = $wgTitle->getDBkey();
                break;
            }
            break;
          case 'user':
            $wgUser = \RequestContext::getMain()->getUser();
            switch ($sParam) {
              case 'login':
                $out = $wgUser->getName();
                break;
              case 'dbkey':
                $out = $wgUser->getTitleKey();
                break;
              case 'id':
                $out = $wgUser->getID();
                break;
              case 'can':
                $out = $wgUser->isAllowed($strParts[2]);
                break;
              case 'rights':
                $arRights = $wgUser->getRights();
                $out = '';
                foreach ($arRights as $key=>$val) {
                    $out .= '\\'.$val;
                }
                break;
              case 'email':
                $out = $wgUser->getEmail();
                break;
              case 'name':
                $out = $wgUser->getRealName();
                break;
              default:
                $out = '?'.$strParam;
            }
            break;
          case 'w3ext':
            switch ($sParam) {
              case 'docurl':  // URL for documentation
                $out = cExtension::Me()->DocsURL();
                break;
            }
            break;
        }

        return $out;
    }
    
    // -- PARSING -- //
    // ++ DEBUGGING ++ //
    
    static public function DumpAll() {
        $arVars = self::GetVars();
        if (count($arVars) > 0) {
            $out = "<li><b>Variables</b>:\n<ul class='dump'>\n";
            foreach ($arVars as $oVar) {
                $out .= $oVar->DumpSelf();
            }
            $out .= "</ul>\n";
        } else {
            $out = "<li><i>No variables are set.</i>\n";
        }
        return $out;
    }
}  
/*::::
  THINKING:
    DefaultClass(): get_called_class() is the static equivalent for get_class($this).
      It seems intuitively correct that any given Var-type should default to spawning
      more of itself -- though perhaps this assumption should be examined later.
      In any case, it can be overridden.
 */
class cVar extends csVar {

    // ++ CONFIG ++ //
  
    static protected function DefaultClass() : string { return get_called_class();	}
    
    // -- CONFIG -- //
    // ++ ACTIONS ++ //

    /*----
      ACTION: Clears the variable's value from page storage and removes any array elements
    */
    public function ClearStored() {
        $this->Fetch();	// make sure any stored array elements are loaded so they don't get overlooked
        if ($this->HasNodes()) {
            $ar = $this->GetNodes();
            foreach ($ar as $key => $oVar) {
            $oVar->SetValue(NULL);
            $oVar->ClearStored();
            }
        }
        $this->SetValue(NULL);
        $this->Store();
    }

    // -- ACTIONS -- //
    // ++ ATTRIBUTES ++ //

    private $sName;
    protected function SetName($sName,$sIndex=NULL) {
        $this->sName = $sName;
        return;	// 2016-11-07 writing still in progress; fail gracefully
        $this->SetIndex($sIndex);
    }
    // PUBLIC so caller can store it externally
    public function GetName() { return $this->sName; }
    /*----
      RETURNS: full variable name -- including array index, if present
      HISTORY:
	2011-09-19 crude implementation so we can store array indexes in page properties
	2017-12-14 I think for now I decided that vars would be flat, not a tree; removing "parent" references
    */
    public function GetStorageName() { return $this->GetName(); }
    
    // -- ATTRIBUTES -- //
    // ++ VALUE ACCESS ++ //
    
    private $sValue;
    // PUBLIC because tags need to be able to alter variable values
    public function SetValue($s=NULL) { $this->sValue = $s; }
    public function GetValue() { return $this->sValue; }
    
    // -- VALUE ACCESS -- //
    // ++ DATA WRITE ++ //
    
    public function SaveValue_toCurrentPage(\Parser $mwoParser) {
        global $wgTitle;
        
        $oProps = new F\mw\ctProperties_page($mwoParser,$wgTitle);
        $oProps->SaveValue($this->GetStorageName(),$this->GetValue());
    }
    
    // -- DATA WRITE -- //
    // ++ DEBUGGING ++ //
    
    protected function DumpSelf() {
        $sName = $this->GetName();
        $sValue = $this->GetValue();
        $htValue = htmlspecialchars($sValue);
        $out = "<li>[<b>$sName</b>] = [<i>$htValue</i>]\n";
    
        return $out;
    }

    // -- DEBUGGING -- //
}
