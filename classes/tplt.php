<?php namespace w3tpl;
/*
  PURPOSE: specialized string template
  HISTORY:
    2017-11-09 rewriting for w3tpl2
    2022-04-08 renamed from w3tpl\cStringTemplate to w3tpl\cTemplate
*/

use ferret as F;

class cTemplate extends F\strings\caTemplate {
    use F\taArrayReader;
    use F\taArrayReadOnly;

    // CEMENT
    #public function SetVariableValue($sName,$sValue) { csVar::SetVariableValue($sName); }
    #public function GetVariableStatus($sName) : F\cElementResultExtended { return csVar::VariableStatus($sName); }

    public function GetCells() : array { return csVar::GetValues(); }

}
