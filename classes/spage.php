<?php namespace w3tpl;
/*
  PURPOSE: SpecialPage for w3tpl operations that need it
  HISTORY:
    2022-04-17 created
*/

use ferret as F;

cEntryPoint::Setup();

class cSpecialPage extends \SpecialPage {
    use F\mw\tSpecialPage;

    // ++ SETUP ++ //
    
    // OVERRIDE
    public function __construct() { parent::__construct('w3tpl'); }
    
    // -- SETUP -- //
    // ++ CONFIG ++ //
    
    /*----
      OVERRIDE, MW CALLBACK (I think)
      PURPOSE: Defines text to display on Special:SpecialPages
      NOTE: There's got to be a way to do this in extension.json, but I don't know what it is.
    */
    function getDescription() { return 'W3TPL form-target page'; }
    
    // -- CONFIG -- //
    // ++ DATA ++ //
    
    private $oArgs = NULL;
    protected function RequestArgs() : F\cArrayLocal {
        if (is_null($this->oArgs)) {
            global $wgRequest;
          
            $arArgs = $wgRequest->getValues();
            // The "title" argument is always received - remove that
            #$sTitle = $arArgs['title']; // save page title for later display
            unset($arArgs['title']);
        
            $oArgs = new F\cArrayLocal;
            $oArgs->SetArray($arArgs);
            $this->oArgs = $oArgs;
        }
        return $this->oArgs;
    }
    
    // -- DATA -- //
    // ++ EVENTS ++ //

    protected function Go(?string $sSubPage) {
        global $wgOut;

        $doHideDiags = FALSE;
        switch ($sSubPage) {
          case 'makepage':
            if ($this->MakePageReady()) {
                $this->MakePage();
                $doHideDiags = TRUE;
            }
            break;
          #default:
        }

        $ftDiags =
          $this->RenderRequestDiags()       // show data from the POST request
          .$this->RenderExtensionDiags()    // show basic W3TPL status info
          .$this->RenderVariables()         // show any variables defined
          ;

        if ($doHideDiags) {
            $out = F\cEnv::HideDrop('Diagnostics',$ftDiags);
        } else {
            $out = $ftDiags;
        }
          
        $wgOut->AddHTML($out);	// display error diagnostic
    }
    
    // -- EVENTS -- //
    // ++ COMMANDS ++ //
    
    protected function MakePage() {
        global $wgOut;

        $wgOut->addHTML('Doing MakePage()...<br>');
        
        $oArgs = $this->RequestArgs();
        $sTpltPage	= $oArgs->Cell('!TPLTPAGE')->GetItNz();	// page containing template data

        if (is_null($sTpltPage)) {
            // no page specified
            // TODO: handle this
            $wgOut->addHTML('No template page specified!');
        } else {
            $wgOut->addHTML("Template page: [$sTpltPage]<br>");
            // load the contents of the specified page
            $omwTpltTitle = \Title::newFromText($sTpltPage);
            #$omwTpltArticle = new Article($owmTitleTplt);
            $omwTpltPage = new \WikiPage($omwTpltTitle);
            $omwTpltContent = $omwTpltPage->getContent();
            #$sNewText = $mwoTpltContent->getWikitextForTransclusion();	// I hope this works...

            #$htDbg = F\csClassDebugger::DumpMethods($omwTpltPage);
            #$wgOut->addHTML($htDbg);
            
            $omwUser = \RequestContext::getMain()->getUser();
            $omwParsOpts = \ParserOptions::newFromUser($omwUser);
            $omwTpltParsOut = $omwTpltPage->getParserOutput($omwParsOpts);   // ParserOutput
            $htTpltPage = $omwTpltParsOut->getText();
            
            // w3tpl code on the template page has now been executed as if transcluded but not shown.
            
            // get the template strings for title and contents:
            $osTpltTitle = csVar::VariableStatus('!newtitle');
            $osTpltText = csVar::VariableStatus('!newtext');
            
            #echo 'Variables: '.csVar::DumpFull();
            
            $ok = TRUE;
            $sMsg = '';
            if (!$osTpltTitle->HasIt()) {
                $ok = FALSE;
                $sMsg .= 'Missing form value: !newtitle<br>';
            }
            if (!$osTpltText->HasIt()) {
                $ok = FALSE;
                $sMsg .= 'Missing form value: !newtext<br>';
            }
            if ($ok) {
                $sTpltTitle = $osTpltTitle->GetIt();
                $sTpltText = $osTpltText->GetIt();
                $mwoParser = \MediaWiki\MediaWikiServices::getInstance()->getParserFactory()->create();
                #echo "TITLE=[$sTpltTitle]<br>";
                /* 2022-05-03 actually, I think these now arrive pre-parsed...
                $sNewTitle = $mwoParser->recursiveTagParse($sTpltTitle);
                $sNewText =  $mwoParser->recursiveTagParse($sTpltText);
                */
                $sNewTitle = $sTpltTitle;
                $sNewText = $sTpltText;
                
                $wgOut->setPageTitle( $sNewTitle );
                $omwNewTitle = \Title::newFromText( $sNewTitle );
                if (is_object($omwNewTitle)) {
                    $sFromURL = F\csHTTPEnv::ClientFromURL();
                    $sEditSumm = "Created by W3TPL, from [$sFromURL]+[[$sTpltPage]].";

                    // generate the new page preview
                    
                    $omwNewArticle = new \Article($omwNewTitle);
                    $omwNewEdit = new \EditPage($omwNewArticle);
                    $sNewTitle_check = $omwNewEdit->mTitle->getPrefixedText();
                    $wgOut->AddWikiTextAsInterface("'''New Page Title''': [[$sNewTitle_check]]");
                    $wgOut->AddWikiTextAsInterface("'''Preview''': <hr>\n$sNewText<hr>");

                    $omwNewEdit->textbox1 = $sNewText;
                    $omwNewEdit->preview = TRUE;
                    $s = F\cEnv::BoldIt('Make final changes to the text here, and save to create the page:');
                    $wgOut->AddWikiTextAsInterface($s);
                    #global $wgTitle;
                    #$wgTitle = $omwNewTitle;	// make the form post to the page we want to create
                    $omwNewEdit->action = 'submit';
                    $omwNewEdit->contentModel = CONTENT_MODEL_WIKITEXT;
                    $omwNewEdit->summary = $sEditSumm;
                    // 2022-04-11 Apparently we need to do this now, but I'm guessing as to what Title is best.
                    $omwNewEdit->setContextTitle($omwTpltTitle);
                    $omwNewEdit->showEditForm();
                    
                } else {
                    $ok = FALSE;
                    $sMsg = "Could not create Title object from title string [$sNewTitle].";
                }
            }
            if (!$ok) {
                $wgOut->AddHTML('Could not create page &mdash;<br>'.$sMsg);
            }
        }
    }
    // RETURNS: TRUE iff conditions are right to run MakePage()
    protected function MakePageReady() : bool {
        return $this->RequestArgs()->CellCount() > 0; // simplest criterion, for now
    }
    
    // -- COMMANDS -- //
    // ++ DIAGNOSTICS ++ //

    protected function RenderExtensionDiags() : string {
        $arTags = caTag::TagsRegistered();
        ksort($arTags);
        if (count($arTags) == 0) {
            $htList = "<b>Warning</b>: No tags have been enabled!";
        } else {
            $htList = "<ul>\n";
            foreach ($arTags as $sName => $sClass) {
                $htList .= "  <li>&lt;<b>$sName</b>&gt;</li>\n";
            }
            $htList .= "</ul>\n";
        }

        $oExt = cExtension::Me();
        $sName = $oExt->Name();
        $sVer = $oExt->Version();
        $sDocURL = $oExt->DocsURL();
        $out = <<<__END__
<h2>Extension Status</h2>
<p><a href="$sDocURL">$sName</a> version <b>$sVer</b></p>
<p>Available tags:</p>
$htList
__END__;
        return $out;
    }
    protected function RenderRequestDiags() : string {
        $oArgs = $this->RequestArgs();

        if ($oArgs->CellCount() > 0) {
            $arArgs = $oArgs->GetArray();
        
            $htList = "<ul>\n";
            foreach ($arArgs as $sName => $sValue) {
                $htVal = htmlspecialchars($sValue);
                $htList .= "  <li><b>$sName</b> = [$htVal]</li>\n";
            }
            $htList .= "</ul>\n";
            $out = "<h2>Request data received</h2><p>$htList</p>";
        } else {
            $out = '<p>No form data has been submitted.</p>';
        }
        return $out;
    }
    protected function RenderVariables() : string {
        return
          'Variables defined:'
          .csVar::DumpFull()
          ;
    }
}
