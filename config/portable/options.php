<?php namespace w3tpl;
/*
  PURPOSE: Load this file if you need to set any options from LocalSettings.php.
  THINKING: The MediaWiki JSON-based extension invocation apparently does not register classes for autoloading,
    so we have to explicitly load the options class before we can use it.
  HISTORY:
    2020-08-14 created for tidying/update of w3tpl
*/
class cOptions {
    static private $bAllowRawHTML = FALSE;
    static public function GetAllowRawHTML() : bool { return self::$bAllowRawHTML; }
    static public function SetAllowRawHTML(bool $b) { self::$bAllowRawHTML = $b; }
    
    static private $bProvideSMW = FALSE;
    static public function GetProvideSMWSupport() : bool { return self::$bProvideSMW; }
    static public function SetProvideSMWSupport(bool $b) { self::$bProvideSMW = $b; }
    
    static private $bStrictMode = FALSE;
    static public function GetIsStrictMode() : bool { return self::$bStrictMode; }
    static public function SetIsStrictMode(bool $b) { self::$bStrictMode = $b; }
    
    static private $sTemplatePrefix = '[$';
    static public function GetTemplatePrefix() : string { return self::$sTemplatePrefix; }
    static public function SetTemplatePrefix(string $s) { self::$sTemplatePrefix = $s; }
    static private $sTemplateSuffix = '$]';
    static public function GetTemplateSuffix() : string { return self::$sTemplateSuffix; }
    static public function SetTemplateSuffix(string $s) { self::$sTemplateSuffix = $s; }
}

include_once dirname(__DIR__).'/links/local/override.php';
