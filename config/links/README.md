The "links" folder needs to contain the following:
* "ferreteria": a link to the local ferreteria deployment folder (created by git clone)
* "local": a link to w3tpl's local config folder (typically {user home}/site/config/w3tpl/local
