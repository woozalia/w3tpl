<?php namespace w3tpl\tag;
/*
  TAG: <hide>
  ACTION: Doesn't display anything between the tags. Basically does nothing.
    Any tag that wants to be able to display certain things *anyway* must handle output directly.
  HISTORY:
    2012-01-15 returning NULL now causes UNIQ-QINU tag to be emitted; changing to '' seems to fix this.
    2017-10-29 adapting from old-style code
*/

use ferret as F;
use w3tpl as W3;

class c_hide extends W3\caTag_basic {
    // ++ CONFIG ++ //

    // CEMENT
    protected function TagName() : string { return 'HIDE'; }
    
    // -- CONFIG -- //
    // ++ INPUT ++ //

    protected function GetTagInput() : string { return $this->GetInput(); }
    protected function UseTagInput(string $sInput) : string {
        $ftOutput = '';

        // always parse the input, because there may be stuff that needs to happen
        $mwoParser = $this->GetParser();
        $ftInput = $mwoParser->recursiveTagParse( $sInput );
        
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
              case 'detsumm': // use <details>/<summary> tags to hide
                $ftOutput = F\cEnv::HideDrop($sVal,$ftInput);
                break;
            }
        }
        return $ftOutput;
    }
    
    // -- INPUT -- //
}
