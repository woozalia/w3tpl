<?php namespace w3tpl\tag;
/*
  TAG: <if>
*/

use w3tpl as W3;

class c_else extends W3\caTag {
    // ++ PROPERTIES ++ //

    // CEMENT
    protected function TagName() : string { return 'ELSE'; }
    
    // -- PROPERTIES -- //
    // ++ EVENTS ++ //
    
    public function Go() : string {
	$arArgs = $this->GetArguments();
	    
	// WRITING IN PROGRESS
    }
}
