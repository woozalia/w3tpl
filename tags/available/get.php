<?php namespace w3tpl\tag;
/*
  TAG: <get>
  DOCS: https://wooz.dev/W3TPL/tags/get
*/

use ferret as F;
use w3tpl as W3;

class c_get extends W3\caTag_Var {

    // ++ PROPERTIES ++ //

    // CEMENT
    protected function TagName() : string { return 'GET'; }
    
    // -- PROPERTIES -- //
    // ++ INPUT SOURCE ++ //

    protected function GetTagInput() : string {
        $sInput = '';
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            $isFound = TRUE;
            switch ($sName) {
              case 'name':  // source is a variable
                $osVar = W3\csVar::VariableStatus($sVal);
                if ($osVar->HasIt()) {
                    $sInput = $osVar->GetIt();
                } else {
                    $sInput = "ERROR: there is no variable named [$sVal]."; // TODO 2022-04-22 format as error
                }
                break;
              default:
                $isFound = FALSE;
            }
            if ($isFound) {
                // return on first match
                return $sInput;
            }
        }
        // if no matches here, check standard attributes:
        return parent::GetTagInput();
    }

    /* 2022-04-20 rewriting
    public function Go() : string {
        $sVarName = $this->RequireArgument('name');
        #$sIndex = $this->ArgumentValueNz('index');
        $doLoad = $this->ArgumentExists('load');
        $doRaw = $this->ArgumentExists('raw');  // load non-serialized
        $doFmt = $this->ArgumentExists('format');
        
        $oVar = W3\csVar::GetVariable_FromExpression($sVarName,TRUE);
        #echo 'GET - START: ['.$oVar->GetName()."]: [".htmlspecialchars($oVar->GetValue())."]<br>";
        if ($doLoad) {
            #echo 'GOT TO '.__FILE__.' line '.__LINE__.'<br>';
            global $wgTitle;	// 2018-02-10 surely there is somewhere better to get this?
            
            $oProps = new F\mw\ctProperties_page($this->GetParser(),$wgTitle);
            //$oVar->Save($oProps);
            $sName = $oVar->GetName();
            $sVal = $oVar->GetValue();
            if ($doRaw) {
                $sVar = $oProps->LoadValueRaw($sName);
            } else {
                $sVar = (string)$oProps->LoadValue($sName);
            }
            $oVar->SetValue($sVar);
        } else {
            #echo 'GOT TO '.__FILE__.' line '.__LINE__.'<br>';
            $sVar = $oVar->GetValue();
            if (is_null($sVar)) {
                $sVar = '';
            }
        }
        if ($doFmt) {
            $sFmt = $this->ArgumentValue('format');
            switch ($sFmt) {
              case 'showtag': $sVar = "&lt;$sVar&gt;"; break;
              case 'astag': $sVar = "<$sVar>"; break;
            }
        }
        #echo W3\csVar::DumpFull();
        #$htVal = htmlspecialchars($sVar);
        #echo 'GET - FINAL: ['.$oVar->GetName()."]: [$htVal]<br>";
        return $sVar;
    }
    */
    
    // -- EVENTS -- //

}
