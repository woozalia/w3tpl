<?php namespace w3tpl\tag;
/*
  TAG: <if>
*/

use w3tpl as W3;

class c_if extends W3\caTag {
    // ++ PROPERTIES ++ //

    // CEMENT
    protected function TagName() : string { return 'IF'; }
    
    // -- PROPERTIES -- //
    // ++ EVENTS ++ //

    /*----
      THINKING: $this->GetInput() is the text between <if> and </if>.
        The whole point of this tag is to determine whether or not
        that text gets processed or not.
        
        By default, the text is NOT processed UNLESS attributes are set
        which result in a true evaluation.
      ATTRIBUTES:
        "flag=<expr>": if the expression evaluates to nonzero, then process the textblock
        "not": if present, invert the expression results
    */
    public function Go() : string {
        $arArgs = $this->GetArguments();
        $out = '';
        if (array_key_exists('flag',$arArgs)) {
            $sFlagExpr = $arArgs['flag'];
            $bFlagValue = W3\csVar::GetValue_fromExpression($sFlagExpr) ? TRUE : FALSE;
            // WRITING IN PROGRESS
            
            $bInvert = $this->ArgumentExists('not');
            #$bFlagValue = FALSE;
            #$bInvert = TRUE;
            /*
            if ($bFlagValue) {
                if ($bInvert) {
                    $bGo = FALSE;
                } else {
                    $bGo = TRUE;
                }
            } else {
                if ($bInvert) {
                    $bGo = TRUE;
                } else {
                    $bGo = FALSE;
                }
            }*/
            // NOTE: parens are very important here. PHP prioritizes "=" over "xor". #WTF
            $bGo = ($bFlagValue xor $bInvert);
            #echo "FLAG VALUE=[$bFlagValue] NOT=[$bInvert] RESULT=[$bGo]\n";
            #echo "FALSE xor TRUE=[".(FALSE xor TRUE).']<br>';
            if ($bGo) {
                $sTagInput = $this->GetInput();
                $out .= $this->GetParser()->recursiveTagParse($sTagInput);
            }
        }
        return $out;
    }
}
