<?php namespace w3tpl\tag;
/*
  TAG: <dump>
  HISTORY:
    2017-11-01 starting (loosely adapting from w3tpl1)
*/

use w3tpl as W3;

class c_dump extends W3\caTag {

    // ++ PROPERTIES ++ //

    // CEMENT
    protected function TagName() : string { return 'DUMP'; }
    
    // -- PROPERTIES -- //
    // ++ EVENTS ++ //

    public function Go() : string {
        $doVars = $this->ArgumentExists('vars');
        
        if ($doVars) {
            $out = W3\csVar::DumpAll();
        }
        return $out;
    }
    
    // -- EVENTS -- //
}
