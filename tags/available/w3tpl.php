<?php namespace w3tpl\tag;
/*
  TAG: <w3tpl>
  ACTION: 
    * (FUTURE) parses and executes its contents as a script
    * Provides certain internal functions that don't work with the @sysdata format.
  HISTORY:
    2022-05-01 started
*/

use ferret as F;
use w3tpl as W3;

class c_w3tpl extends W3\caTag_basic {
    // ++ CONFIG ++ //

    // CEMENT
    protected function TagName() : string { return 'W3TPL'; }
    
    // -- CONFIG -- //
    // ++ EVENT ++ //

    // OVERRIDE caTag_basic
    public function Go() : string {
        $sIn = $this->GetTagInput();
        $this->GetTagAttribs();
        $sOut = $this->UseTagAttribs($sIn);
        return $sOut;
    }
    // ++ INPUT ++ //

    protected function GetTagInput() : string { return $this->GetInput(); }
    protected function GetTagAttribs() {
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
            }
        }
    }
    protected function UseTagAttribs(string $sInput) : string {
        $ftOutput = '';

        // always parse the input, because there may be stuff that needs to happen
        $mwoParser = $this->GetParser();
        $ftInput = $mwoParser->recursiveTagParse( $sInput );
        
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
              case 'detsumm': // use <details>/<summary> tags to hide
                $ftOutput = F\cEnv::HideDrop($sVal,$ftInput);
                break;
            }
        }
        return $ftOutput;
    }
    
    // -- INPUT -- //
}
