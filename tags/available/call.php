<?php namespace w3tpl\tag;
/*
  TAG: <call>
*/

use w3tpl as W3;

class c_call extends W3\caTag {
    // ++ PROPERTIES ++ //

    // CEMENT
    protected function TagName() : string { return 'CALL'; }
    
    // -- PROPERTIES -- //
    // ++ EVENTS ++ //
    
    public function Go() : string {
        $arArgsRaw = $this->GetArguments();
        $pcnt = 0;
        $arArgs = array();
        foreach ($arArgsRaw as $sNameRaw => $sValueRaw) {
            $sName = strtolower(trim($sNameRaw));
            if ($pcnt) {
                // every arg except the first one
                $sValue = W3\csVar::GetValue_fromExpression($sValueRaw);
                $arArgs[$sName] = $sValue;
            } else {
                // we're looking at the very first arg -- must be the function's name
                if ($sName == 'func') {	// might be given as "func=funcname"
                    $sFuncName = strtolower(W3\csVar::GetValue_fromExpression($sValueRaw));
                } else {		// ...but just naming the function as the first arg (no value) is also ok
                    $sFuncName = strtolower(W3\csVar::GetValue_fromExpression($sNameRaw));
                }
            }
            $pcnt++;
        }
        $out = NULL;
        $oFunc = new xcFunc($this->GetParser(),$sFuncName);
        $oFunc->Fetch();		// retrieve fx() definition from database
        
        $sInput = $this->GetInput();
        if ($sInput) {
            // pass the data between <call ...> and </call> to the function as a special argument
            $arArgs['*tag_input'] = $sInput;	// TODO: make *tag_input a constant
        }
        
        $oFunc->SetArguments($arArgs);	// pass arguments to use for parameters

    //	if ($pcnt) {
            $out .= $oFunc->Execute();
    /*	} else {
            $wgOut->AddHTML('<span class=previewnote><strong>W3TPL ERROR</strong>: Function "'.$strName.'" not loaded; probably an internal error.</span>');
        }	// 2017-11-05 this was skipping fx() execution if there were no arguments, which doesn't make sense */
        
        return $out;
    }
}
