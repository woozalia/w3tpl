<?php namespace w3tpl\tag;
/*
  TAG: <let>
  DOCS: https://wooz.dev/W3TPL/tags/let
*/

use ferret as F;
use w3tpl as W3;

class c_let extends W3\caTag_Var {

    // ++ PROPERTIES ++ //

    // CEMENT
    protected function TagName() : string { return 'LET'; }
    
    // -- PROPERTIES -- //
    // ++ INTERNAL VALUES ++ //

    protected $sVarName = NULL; // this can also be an expression
    protected function GetVariableName() : string {
        if (is_null($this->sVarName)) {
            $this->sVarName = $this->RequireArgument('name');
        }
        return $this->sVarName;
    }
    
    private $oVar = NULL;
    protected function GetVariableObject() : W3\cVar {
        if (is_null($this->oVar)) {
            $this->oVar = W3\csVar::GetVariable_FromExpression($this->GetVariableName(),TRUE);
        }
        return $this->oVar;
    }
    
    // -- INTERNAL VALUES -- //
    // ++ INPUT SOURCE ++ //
    
    /*----
      THINKING: Yes it's a little bit silly to do this with an array when there's only one entry.
        but it seems a good idea to keep to a standard form -- both for maintainability and also
        in case we add more options later.
        
        Also, this phase is where we get the input *from*, so there's no point in looking for more
          of these tags once we find one; can't get input from more than one place. (I mean, we
          *could*, but it could easily result in ambiguous or unclear behavior.
    */
    protected function GetTagInput() : string {
        $sInput = '';
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            $isFound = TRUE;
            switch ($sName) {
              case 'self':
                $oVar = $this->GetVariableObject();
                $sInput = $oVar->GetValue();
                break;
              default:
                $isFound = FALSE;
            }
            if ($isFound) {
                // return on first match
                return $sInput;
            }
        }
          
        // if no matches here, check standard attributes:
        return parent::GetTagInput();
    }

    // -- INPUT SOURCE -- //
    // ++ INPUT PROCESSING ++ //
    
    protected function UseTagInput(string $sInput) : string {
        $sRes = $sInput;  // by default, resulting value is tag input
        $sOut = '';       // by default, no page output 
        $arArgs = $this->GetArguments();
        foreach ($arArgs as $sName => $sVal) {
            switch ($sName) {
              case 'echo':   // echo
                $sOut = $sRes;
                break;
            }
        }
        // process standard attributes
        $sRes = parent::UseTagInput($sRes);
        
        // set named variable's value to processing result
        $oVar = $this->GetVariableObject();
        $oVar->SetValue($sRes);
        
        return $sOut; // return output to display, if any
    }
      
    // -- INPUT PROCESSING -- //
}
