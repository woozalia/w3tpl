<?php namespace w3tpl;
/*----
  PURPOSE: wiki-based blogging for w3tpl
    This version is pure MW (using page properties stored via w3tpl)
  CONVENTIONS:
    MWO = MediaWiki Object
    W3O = W3TPL Object
  HISTORY:
    2018-02-10 Created for VbzWiki (and should also work with HypertWiki, I think?)
*/

class xcModule_Blogging extends xcModule {

    // ++ TAG API ++ //

    public function TagAPI_RenderBlogAbstracts_forAll(array $arArgs) {
        $mwoTitles = $this->FetchBlogPostTitles();
        $arTitles = NULL;
        foreach ($mwoTitles as $mwoTitle) {
            $oPage = new cBlogEntry($this,$mwoTitle);
            $oTitleProps = $oPage->GetPropertyStorage();
            $sDate = $oTitleProps->GetFieldValueEncoded('timestamp');
            // ideally, we'd parse this into a numeric, but for now...
            $arTitles[$sDate] = $oPage;
        }
        if (is_null($arTitles)) {
            $out = "<i>No blog entries found for user '$sUserFilt'.</i>".$dbg;
        } else {
            $out = NULL;
            ksort($arTitles);
            foreach ($arTitles as $sDate => $oPage) {
                $out .= $oPage->RenderSummary();
            }
        }
        return $out;
    }
    public function TagAPI_RenderUserBlogList(array $arArgs) { $this->TagAPI_RenderBlogAbstracts_forUser($arArgs); }  // DEPRECATED ALIAS
    /*----
      ACTION: Render a user's blog summary list
      HISTORY:
        2018-02-11 started, for VbzWiki
    */
    public function TagAPI_RenderBlogAbstracts_forUser(array $arArgs) {
        global $wgTitle;
        
        // There might be a better way to do this, but I just need to get this working:

        // 1. Get all titles that are blog posts
        /*
        $mwoCatPost = \Category::newFromName('Data/blog/post');
        $mwoTitles = $mwoCatPost->getMembers();	// this can later be modified to support paging through long lists
        */
        $mwoTitles = $this->FetchBlogPostTitles();

        // 1a. Get the user whose posts we want to show
        $mwoTitle = $wgTitle;	// for now, we'll just assume that the blog list page is in userspace
        $sUserFilt = strtolower($mwoTitle->getRootText());  // TODO: make this overrideable
        
        // 2. Go thorough complete list to get just titles where the author is the current user
        //	we'll take this opportunity to sort the entries as well

        $arTitles = NULL;
        $dbg = NULL;
        #$tTitles = \fcApp::Me()->GetDatabase()->MakeTableWrapper(__NAMESPACE__.'\\ctBlogEntries');
        
        foreach ($mwoTitles as $mwoTitle) {
            $oPage = new cBlogEntry($this,$mwoTitle);
            $oTitleProps = $oPage->GetPropertyStorage();
            
            // 2020-09-24 is this actually necessary? (#1)
            $sUserEntry = strtolower($oTitleProps->GetFieldValueEncoded('user'));

            $dbg = "ENTRY=[$sUserEntry] FILT=[$sUserFilt]<br>";
            if ($sUserEntry == $sUserFilt) {
                // yes, this is the right user
                $sDate = $oTitleProps->GetFieldValueEncoded('timestamp');
                // ideally, we'd parse this into a numeric, but for now...
                $arTitles[$sDate] = $oPage;
            }
        }
            
        if (is_null($arTitles)) {
            $out = "<i>No blog entries found for user '$sUserFilt'.</i>".$dbg;
        } else {
            $out = NULL;

            /* might be more useful in another context; was mainly for debugging
            $n = count($arTitles);
            $sPlur = \fcString::Pluralize($n,'y','ies');
            $out = "<i>Found $n blog entr$sPlur for user '$sUserFilt'.</i>$dbg"; */

            ksort($arTitles);
            
            foreach ($arTitles as $sDate => $oPage) {
                $out .= $oPage->RenderSummary();
            }
        }
        return $out;
    }
    /*----
      ACTION: Render the blog entry defined by the current page
      HISTORY:
        2018-02-10 created for VbzWiki
    */
    public function TagAPI_RenderFullPost(array $arArgs) {
        /* 2020-10-06 old code
        $oBlogPage = new cBlogEntry($this);
        $oBlogPage->Use_GlobalTitle();
        */
        global $wgTitle;
        $oBlogPage = new cBlogEntry($this,$wgTitle);
        return $oBlogPage->RenderPage();
    }

    // -- TAG API -- //
    // ++ TABLE ++ //
    
    protected function BlogEntryTable() { return $this->GetDatabase()->GetTableWrapper(__NAMESPACE__.'\\xcBlogEntry');  }

    // -- TABLE -- //
    // ++ MEDIAWIKI ++ //
    
    /*----
      LATER: modify to support paging through long lists
    */
    protected function FetchBlogPostTitles() : \TitleArray {
        $mwoCatPost = \Category::newFromName('Data/blog/post');
        $mwoTitles = $mwoCatPost->getMembers();   // getMembers($limit = false,$offset = '')
        return $mwoTitles;
    }
    // -- MEDIAWIKI -- //
}
class ctBlogEntries extends \ferreteria\mw\ctTitles {
}
#class xcBlogEntry extends \fcPageData_MW {
class cBlogEntry extends \ferreteria\mw\crcTitle {

    // ++ SETUP ++ //

    public function __construct(xcModule $oMod, \Title $mwo=NULL) {
        $this->SetModuleW3O($oMod);
        $this->SetTitleMWO($mwo);
    }
    
    // -- SETUP -- //
    // ++ FRAMEWORK ++ //

    private $oMod;
    protected function SetModuleW3O(xcModule $oMod) { $this->oMod = $oMod; }
    protected function GetModuleW3O() { return $this->oMod; }
    
    private $mwoPOutput=NULL;
    protected function GetMWParserOutput() {
        if (is_null($this->mwoPOutput)) {
            $this->mwoPOutput = xcModule::GetParser()->getOutput();
        }
        return $this->mwoPOutput;
    }
    /*----
      TODO: This should probably be provided at a higher level somewhere
      THINKING: I guess the intent here is to translate from a nonstandard way of 
        representing categories in text into how the page is rendered?
      HISTORY:  
        2020-09-24 For now, let's assume that normalizing the name is unnecessary.
    */
    protected function AddCategory($sName,$sSort) {
        #$wtName = \fcDataConn_MW::NormalizeTitle($sName,NS_CATEGORY);
        $mwoPOutput = $this->GetMWParserOutput();
        $mwoPOutput->addCategory($sName,$sSort);
    }

    // -- FRAMEWORK -- //
    // ++ OUTPUT ++ //

    /*----
      ACTION: Render just the blog entry summary, as in a list on a front page
      NOTE: This directly accesses property data instead of using MW API fx()
        because the latter only works for the page currently being edited.
    */
    public function RenderSummary() {
        $oTitleProps = $this->GetPropertyStorage();
        
        // 2020-09-24 Is this actually necessary? (#2) We don't need to list unknown values...
        #$oProps->LoadPropertyValues();	// load all the values for this page
        
        $sTitleText = $oTitleProps->GetFieldValueEncoded('title');	// title for blog entry

        $mwoTitle = $this->GetTitleMWO();
        $urlTitle = $mwoTitle->getFullURL();
        $sTitleFull = $mwoTitle->getFullText();
        $htTitle = "<a href='$urlTitle'>$sTitleText</a>";
        $wtTitle = "[[$sTitleFull|$sTitleText]]";

        $sWhen = $oTitleProps->GetFieldValueEncoded('timestamp');
        $dtWhen = strtotime($sWhen);
        $ftWhen = date('Y/m/d H:i (l)',$dtWhen);

        $txtAbove = $oTitleProps->GetFieldValueEncoded('textabove');	// this is the "preview"
        $w3oModule = $this->GetModuleW3O();
        $htAbove = $w3oModule->Parse_WikiText($txtAbove);

        $sUser = $oTitleProps->GetFieldValueEncoded('user');
        $mwoUser = \User::newFromName($sUser);
        
        if ($mwoUser === FALSE) {
            // couldn't get the object
            $htUser = "[?user $sUser]";
        } else {
            $mwoUTitle = $mwoUser->getUserPage();
            $urlUser = $mwoUTitle->getFullURL();
            $htUser = '<a href="'.$urlUser.'">'.$sUser.'</a>';
        }
        $wtHeader = $w3oModule->Parse_WikiText("===$wtTitle===");

        $out = <<<__END__
$wtHeader
<span class=blog-excerpt-meta><span class=blog-excerpt-author>$htUser
</span> (<span class=blog-excerpt-timestamp>$ftWhen</span>)</span>
$htAbove
<b><i><a href="$urlTitle">more...</a></i></b>
__END__;

        return $out;
    }
    /*----
      ACTION: Render the current page as a blog page
      ASSUMES: certain standard page properties will be present (need to document these)
    */
    public function RenderPage() {
        $oProps = $this->GetPropertyStorage();
        $sTitle = $oProps->GetFieldValueEncoded('title');	// title for blog entry
        $sUser = $oProps->GetFieldValueEncoded('user');
        $sTime = $oProps->GetFieldValueEncoded('timestamp');
        $sAbove = $oProps->GetFieldValueEncoded('textabove');
        $sBelow = $oProps->GetFieldValueEncoded('textbelow');

        if (is_null($sTitle)) {
            throw new \exception('Could not retrieve value for "title".');
        }
        
        $sCats = $oProps->GetFieldValueEncoded('topicsglobal');
        if (!is_null($sCats)) {
            $arCats = \fcString::Xplode($sCats);
            if (is_array($arCats)) {
                foreach ($arCats as $sCat) {
                    $this->AddCategory($sCat,$sTitle);
                }
            }
        }
        
        $this->AddCategory('data/blog/post',$sTitle);
        $this->AddCategory("author/$sUser",$sTitle);
        
        $out = <<<__END__
==$sTitle==
''<small>posted at $sTime</small>''

$sAbove
----
$sBelow
__END__;
    
        return $this->GetModuleW3O()->Parse_WikiText($out);
   }

    // -- OUTPUT -- //
}
