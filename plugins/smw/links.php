<?php namespace w3tpl;
/*
  PURPOSE: SMW listing management functions for w3tpl
    (2018-01-28) Yeah, but why Google+ Communities?
  REQUIRES: filed-links.php
  TODO: SiteGroupListing() is currently hard-wired for Google+
  HISTORY:
    2011-10-16 w3tpl code started to get too ugly, so pushing out some functionality into callable modules.
    2012-01-24 split off SMW stuff from filed-links.php to smw-links.php
    2013-02-14 w3f_SiteGroupListing() now works for G+
    2015-03-14 removed explicit check for needed library
    2018-01-22 update to work with latest w3tpl
*/

$csModuleClass = 'xcModule_SMWLinks';

class xcModule_SMWLinks extends xcModule {

    // ++ TAG API ++ //

    public function TagAPI_SiteGroupListing(array $iArgs) {
        $db = \fcApp::Me()->GetDatabase();
        
        /*
          GetPages_forPropVal() now replaced by GetTitleObjects_forPropertyValue(),
            but return format is different.
          2020-07-20 ...and that has been replaced by GetTitleRecords_forIDAndValue().
        */
        throw new \exception('2018-02-02 This will need some rewriting.');
        $ar = $db->GetPages_forPropVal('thing type','site-group');
    /*
        $out = '<pre>';
        foreach ($ar as $key => $arRow) {
            $out .= print_r($arRow,TRUE);
        }
        $out .= '</pre>';
    */

        $out = '<ul>';
        $objPage = new \fcPageData_SMW($this);
        foreach ($ar as $key => $arRow) {
            $idSMW = $arRow['s_id'];	// for future coding reference; not currently used
            $idNSpace = $arRow['s_namespace'];
            $sTitle = $arRow['s_title'];
            $objPage->Use_Title_Keyed($sTitle,$idNSpace);

            $sName = $objPage->GetPropData('name');
            $sSub = $objPage->GetPropData('subtitle');
            $ftSub = is_null($sSub)?'':" - <i>$sSub</i>";
            $sSumm = $objPage->GetPropData('summary');
            $idSG = $objPage->GetPropData('ID');
            $htIdx = $objPage->PageLink('idx');
            $htLink = '<a href="https://plus.google.com/communities/'.$idSG.'">'.$sName.'</a>';
            $out .= "<li>[$htIdx] <b>$htLink</b>$ftSub: $sSumm";
        }
        $out .= '</ul>';

        return $out;
        }
    /*----
      INPUT:
        site: name of site whose users should be listed
        filt: additional SMW terms to include in the filter
      USAGE:
        example: <exec module=smw-links func=SiteUserListing_with_xrefs site="Google+" />
    */
    public function w3f_SiteUserListing_with_xrefs(array $iArgs) {
        $strSite = $iArgs['site'];
        $strFilt = NzArray($iArgs,'filt');
        $smwFilt = "[[thing type::site-account]][[site::$strSite]]$strFilt";

        $dbr =& wfGetDB( DB_SLAVE );
        $db = new clsSMWData($dbr);

        $arFilt = array(
          'PropName'	=> '="Thing type"',
          'Value'	=> '="site-account"',
          );
        $rs = $db->GetPages_forProps($arFilt);

        $out = 'blah';
        while ($rs->NextRow()) {
            $out .= '<pre>'.print_r($rs->Values(),TRUE).'</pre>';
        }
        echo 'got to here';
        return $out;
    }
}
